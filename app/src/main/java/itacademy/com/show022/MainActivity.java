package itacademy.com.show022;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private boolean isBound;
    private Intent intent;

    private MyService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intent = new Intent(this, MyService.class);
    }

    ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MyService.MyBinder binder = (MyService.MyBinder) iBinder;
            service = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };

    public void startMyService(View v) {
        bindService(intent, connection, BIND_AUTO_CREATE);

        /*Intent intent = new Intent(this, MyService.class);
        startService(intent);*/
    }

    public void stopMyService(View v) {
        unbindService(connection);

        /*Intent intent = new Intent(this, MyService.class);
        stopService(intent);*/
    }

    public void playRadio(View v) {
        service.playTrack();
    }

    public void pauseRadio(View v) {
        service.pauseTrack();
    }
}
